package ru.example;

public interface AnonInterface<T1, T2, T3, T4> {

    T1 m1();
    T2 m2();
    T3 m3();
    T4 m4();
}
