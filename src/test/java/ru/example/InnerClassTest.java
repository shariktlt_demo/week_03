package ru.example;


import org.junit.Test;

public class InnerClassTest {

    private String a;
    private String b;
    private String c;

    private InnerClassTest() {
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Const {
        public final static String CONS_VAR = "123";
    }

    public static class Builder {

        private InnerClassTest obj = new InnerClassTest();

        public InnerClassTest build(){
            if(obj.b == null){
                throw new IllegalStateException("b not set");
            }
            return obj;
        }

        public Builder setA(String a){
            obj.a = a;
            return this;
        }

    }

    @Test
    public void test(){



        InnerClassTest a = new InnerClassTest();
        InnerClassTest.Inner aInner = a.new Inner();

        InnerClassTest b = new InnerClassTest();
        InnerClassTest.Inner bInner = b.new Inner();

        a.method(aInner);


    }

    @Test
    public void testHidden(){
        Hidden h = new Hidden();
    }

    void method(Inner inner){

    }

    class Inner {

    }

    public static class StaticInner {

    }
}

class Hidden {

}
