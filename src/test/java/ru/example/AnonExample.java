package ru.example;

public class AnonExample {
    public AnonInterface<String, Integer, Float, Double> anonClassCreator() {
        return new AnonInterface<String, Integer, Float, Double>() {
            @Override
            public String m1() {
                return null;
            }

            @Override
            public Integer m2() {
                return null;
            }

            @Override
            public Float m3() {
                return null;
            }

            @Override
            public Double m4() {
                return null;
            }
        };

    }

    public void funcExample() {
        /*
        Функциональный интерфейс пример с анонимным классом:

        MyFuncInterface printer = new MyFuncInterface() {
            @Override
            public void print(Object obj) {

            }
        };*/
        MyFuncInterface printer = obj -> System.out.println(obj);

        funcUse(printer);
    }

    /**
     * Недоступный для редактирования метод
     *
     * @param printer
     */
    private void funcUse(MyFuncInterface printer) {
        for (int i = 0; i < 10; i++) {
            printer.print(i);
        }
    }
}
