package ru.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GenericExmplain {

    public void oldWay() {
        List stringList = new ArrayList();

        stringList.add("1");
        stringList.add("2");


        stringList.add(new HashMap<String,String>());


        for (int i = 0; i < stringList.size(); i++) {
            Object o = stringList.get(i);
            if(o instanceof String) {
                String current = (String) o;
            }
        }
    }

    public void newWay(){
        List<String> stringList = new ArrayList<>();

        stringList.add("asd");
        // stringList.add(123); compile error

        String cur = stringList.get(0);
    }

    public void genericExample(){
        List<Long> longList = new ArrayList<>();
        List<Double> doubleList = new ArrayList<>();

        consumer(longList);
        consumer(doubleList);
    }

    public void consumer(List<? extends Number> list){}


    static class A { void aMethod(){}}
    static class B extends A{}
    static class C extends B{}
    static class D extends C{}

    void pecs(){
        List<A> aList = new ArrayList<>();
        List<B> bList = new ArrayList<>();
        List<C> cList = new ArrayList<>();


        aList.add(new B());

        consumerExample(aList);
        consumerExample(bList);
        // consumerExample(cList); compile error

        producerExample(aList);
        producerExample(bList);
        producerExample(cList);

    }

    /**
     * Producer extends
     *
     * @param list
     */
    void producerExample(List<? extends A> list){
        A a = list.get(0);
        // B b = list.get(0); compile error
       // C c = list.get(0); compile error

        list.get(0).aMethod();

    }

    /**
     * Consumer Super
     *
     * @param list
     */
    void consumerExample(List<? super B> list){
        // list.add(new A()); compile error
        list.add(new B());
        list.add(new C());
        list.add(new D());
    }


}
