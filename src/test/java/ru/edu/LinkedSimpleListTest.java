package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleListTest {

    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    private SimpleList<String> list ;

    @Before
    public void setUp() throws Exception {
        list = new LinkedSimpleList<>();
    }


    @Test
    public void testList(){
        assertEquals(0, list.size());

        list.add(FIRST);
        assertEquals(1, list.size());

        assertEquals(FIRST, list.get(0));

        list.add(SECOND);
        assertEquals(2, list.size());

        assertEquals(SECOND, list.get(1));

    }

    @Test
    public void testBig(){
        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i));
        }

        for (int i = 0; i < 10; i++) {
            assertEquals("Check for "+i, String.valueOf(i), list.get(i));
        }
    }
}