package ru.edu;

public class LinkedSimpleList<T> implements SimpleList<T>{

    private Node<T> head;
    private Node<T> tail;

    private int size;


    private static class Node<T> {
        Node prev;
        T value;
        Node next;

        public Node(T value) {
            this.value = value;
        }
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(T value) {
        Node<T> node = new Node<>(value);

        if(head == null){
            head = node;
            tail = node;
        }else{


            //  head <-> tailElement <-> tail
            //  head > tailElement <-> newElement < tail
            node.prev = tail;
            tail.next = node;
            tail = node;

        }
        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(int index, T value) {
        findNode(index).value = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(int index) {
        Node<T> node = findNode(index);

        /**
         * следующая конструкция позволяет допустить ошибки
         * Node notTypedNode = new Node("");
         * return (T) notTypedNode.value;
         */

        return node.value;
    }

    private Node<T> findNode(int index) {
        Node<T> node = null;
        if( index < size/2) {
            node = head;

            for (int i = 0; i < index; i++) {
                node = node.next;
            }
        }else{
            node = tail;

            for (int i = 0; i < (size-1) - index; i++) {
                node = node.prev;
            }
        }
        return node;
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(int index) {
        removeElement(index);
        size--;
    }

    private void removeElement(int index) {
        if(size == 1){
            head = null;
            tail = null;
            return;
        }
        if(index == 0 || index == -1){
            Node next = head.next;
            if(next != null){
                next.prev = null;
            }
            head = next;
            return;
        }

        if (index == size -1){
            Node prev = tail.prev;
            prev.next = null;
            tail = prev;
        }else{

            //  [i-1]  <prv[i]next>  [i+1]
            //  [i-1]next> [i+1]
            //  [i-1] <prev[i+1]
            Node remove  = findNode(index);

            remove.prev.next = remove.next;
            remove.next.prev = remove.prev;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(T value) {
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            if(node.value.equals(value)){
                return i;
            }
            node = node.next;
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
