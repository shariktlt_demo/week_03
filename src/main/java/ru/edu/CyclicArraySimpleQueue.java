package ru.edu;

public class CyclicArraySimpleQueue<T> implements SimpleQueue<T> {

    private T[] arr;

    private int head = 0;
    private int tail = 0;


    /**
     *   h
     *  [           ]
     *   t
     *  add(a)
     *   h
     *  [a           ]
     *     t
     *  poll()
     *     h
     *  [            ]
     *     t
     *
     *
     *  add(z)
     *           h
     *  [        w  y ]
     *   t       t
     *
     * @param capacity
     */

    public CyclicArraySimpleQueue(int capacity) {
        arr = (T[]) new Object[capacity];
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(T value) {
        return false;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        return null;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        return null;
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return 0;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return 0;
    }
}
