package ru.edu;

import java.util.ArrayList;
import java.util.Arrays;

public class ArraySimpleList<T> implements SimpleList<T>{

    private T[] arr;

    private int size;

    public ArraySimpleList(int capacity) {

        this.arr = (T[]) new Object[capacity];
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(T value) {
        if(size+1 == arr.length){
            T[] old = arr;
            this.arr = (T[]) new Object[arr.length*2];
            for (int i = 0; i < old.length; i++) {
                arr[i] = old[i];
            }


            /**
             * Для реальных проектов лучше нативные методы, они оптимизированы сразу
             * arr = Arrays.copyOf(arr, arr.length*2);
             */
        }
        arr[size++] = value;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(int index, T value) {

    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(int index) {
        return arr[index];
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(int index) {
        for (int i = index+1; i < size; i++) {
            arr[i-1] = arr[i];
        }
        arr[--size] = null;
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(T value) {
        return 0;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return 0;
    }
}
